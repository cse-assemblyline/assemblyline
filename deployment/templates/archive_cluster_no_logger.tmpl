#!/usr/bin/env python
from urllib import quote
from assemblyline.al.install.seeds.assemblyline_common import seed

IP_CORE = '{ip_core}'
IP_RIAK_NODES = {ips_riak}

SYS_USER = 'al'
FTP_USER = 'ftp_user'
FTP_PASS = '{ftp_pass}'

try:
    from {update_seed_path} import update_seed
    update_seed(seed)
except:
    pass

seed['auth']['internal']['users']['admin']['password'] = '{password}'
seed['auth']['internal']['users'] = {{
    'admin': seed['auth']['internal']['users']['admin']
}}

seed['core']['expiry']['batch_delete'] = True
seed['core']['expiry']['delays'] = {{  # By default we will add 6 month more retention
    "submission": 24*30*6,
    "file": 24*30*6,
    "alert": 24*30*6,
    "result": 24*30*6,
    "error": 24*30*6
}}
seed['core']['expiry']['workers'] = 20
seed['core']['nodes'] = [IP_CORE]
seed['core']['redis']['nonpersistent']['host'] = IP_CORE
seed['core']['redis']['persistent']['host'] = IP_CORE

seed['datastore']['port'] = {ds_port_prefix}087
seed['datastore']['stream_port'] = {ds_port_prefix}098
seed['datastore']['solr_port'] = {ds_port_prefix}093

seed['datastore']['riak']['nodes'] = IP_RIAK_NODES
seed['datastore']['riak']['solr']['heap_max_gb'] = {solr_heap}
seed['datastore']['riak']['solr']['heap_min_gb'] = {solr_heap}
seed['datastore']['riak']['ring_size'] = {ring_size}
seed['datastore']['riak']['nvals'] = {nvals}

seed['datasources'] = {{
    "AL": {{
        "classpath": "assemblyline.al.datasource.al.AL",
        "config": {{}}
    }},
    "Alert": {{
        "classpath": "assemblyline.al.datasource.alert.Alert",
        "config": {{}}
    }}
}}

seed['filestore']['ftp_password'] = FTP_PASS
seed['filestore']['ftp_user'] = FTP_USER
seed['filestore']['support_urls'] = [
    'ftp://{{user}}:{{password}}@{{core}}/opt/al/var/support'.format(core=IP_CORE,
                                                                     user=FTP_USER,
                                                                     password=quote(FTP_PASS))
]
seed['filestore']['urls'] = [
    'ftp://{{user}}:{{password}}@{{core}}/opt/al/var/storage'.format(core=IP_CORE,
                                                                     user=FTP_USER,
                                                                     password=quote(FTP_PASS))
]

seed['installation']['hooks']['ui_pre'].append('al_private.common.hook_ui_pre')
seed['installation']['repositories']['realms']['bitbucket']['branch'] = '{repo_branch}'

seed['services']['master_list'] = {{}}

seed['submissions']['url'] = "https://%s:443" % IP_CORE

seed['system']['name'] = '{sys_name}'
seed['system']['password'] = None
seed['system']['internal_repository'] = {{
    'branch': '{repo_branch}',
    'url': 'http://{{core}}/git/'.format(core=IP_CORE)
}}
seed['system']['user'] = SYS_USER

seed['ui']['audit'] = True
seed['ui']['context'] = "al_private.ui.site_specific.context"
seed['ui']['debug'] = False
seed['ui']['fqdn'] = '{fqdn}'
seed['ui']['read_only'] = True
seed['ui']['secret_key'] = "{secret_key}"

seed['workers']['nodes'] = []
seed['workers']['virtualmachines']['master_list'] = {{}}

if __name__ == '__main__':
    import sys

    if "json" in sys.argv:
        import json
        print json.dumps(seed)
    else:
        import pprint
        pprint.pprint(seed)
