from assemblyline.common.importing import class_by_name
from assemblyline.al.common import forge
from assemblyline.al.common.service_utils import infer_classpath

store = forge.get_datastore()


def service_by_name(n):
    return class_by_name(infer_classpath(store.get_service(n)))
