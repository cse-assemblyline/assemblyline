DEFAULT_REGISTRATION = {
    'enabled': True,
    'hostname': '',
    'ip': '',
    'is_vm': False,
    'mac_address': '',
    'machine_info': {},
    'platform': {},
    'roles': [],
    'service_affinity': {
        'allowed_services': '.*',
        'banned_services': ''
    },
    'vm_host': '',
    'vm_host_mac': ''
}