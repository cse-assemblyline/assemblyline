#!/usr/bin/env python

import os


def install(alsi=None):
    if not alsi:
        from assemblyline.al.install import SiteInstaller
        alsi = SiteInstaller()

    if alsi.using_systemd:
        alsi.sudo_install_file('assemblyline/al/install/etc/systemd/system/orchestrator.service',
                               '/etc/systemd/system/orchestrator.service')

    else:
        alsi.sudo_install_file('assemblyline/al/install/etc/init/orchestrator.conf',
                               '/etc/init/orchestrator.conf')

        if not os.path.exists('/etc/init.d/orchestrator'):
            alsi.runcmd('sudo ln -s /lib/init/upstart-job /etc/init.d/orchestrator')

    alsi.milestone("Orchestrator installed")


if __name__ == '__main__':
    install()
