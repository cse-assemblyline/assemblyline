#!/usr/bin/env python
import argparse
import time

from assemblyline.al.common import forge
config = forge.get_config()

if __name__ == "__main__":
    valid_buckets = ['submission', 'file', 'result']
    parser = argparse.ArgumentParser()
    parser.add_argument('bucket', choices=valid_buckets)
    parser.add_argument('datastore_ips', metavar='datastore_ip', nargs='+')
    args = parser.parse_args()

    bucket = args.bucket

    config.datastore.solr_port = 8093
    config.datastore.stream_port = 8098
    config.datastore.port = 8087
    config.datastore.hosts = args.datastore_ips

    from assemblyline.al.core import datastore
    ds = datastore.RiakStore()

    # Creating fake submissions
    while True:
        for item in ds.stream_search(bucket, "__expiry_ts__:[* TO NOW]"):
            if bucket == "submission":
                ds.delete_submission(item['_yz_rk'])
            elif bucket == "file":
                ds.delete_file(item['_yz_rk'])
            elif bucket == "result":
                ds.delete_result(item['_yz_rk'])

        time.sleep(1)
