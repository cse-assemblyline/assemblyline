#!/usr/bin/env python

import argparse

from assemblyline.al.common import forge
from assemblyline.al.common import log
from assemblyline.al.core.dispatch import Dispatcher
from assemblyline.al.core.servicing import ServiceProxyManager


def main(shard):
    log.init_logging('dispatcher')

    ds = forge.get_datastore()

    service_proxies = ServiceProxyManager(ds.list_service_keys())
    dispatcher = Dispatcher(service_proxies, shard=shard, debug=False)
    dispatcher.start()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--shard', default='0')
    args = parser.parse_args()

    main(args.shard)
