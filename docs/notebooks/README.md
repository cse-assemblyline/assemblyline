# Using Jupyter Notebooks

In order to use ipython/jupyter notebooks, make sure that jupyter notebook is installed on your
appliance or development VM:

    sudo pip install "ipython<6.0" jupyter
    
Some additional "nice to have" packages:

    # Jupyter extensions, for TOC display and code folding
    sudo pip install jupyter_contrib_nbextensions
    
    sudo jupyter contrib nbextension install --system
    
If you'd like to commit changes to the notebook to this git repository:

    sudo pip install nbstripout
    
Run the notebook server as the al user. Take note of the port it's running on and the token. Or you
can set a password to login, see ``jupyter notebook --help`` for additional options

    # Copy the example notebook directory
    sudo -u al cp -r /opt/al/pkg/assemblyline/docs/notebooks /opt/al/ 
    cd /opt/al/notebooks
    sudo -Hu al jupyter notebook
    
Assuming you only have port 22 open to your AL dev machine, set up port forwarding and open 
https://localhost:<localport> in your browser. You'll need the token from when you started jupyter notebook remotely.

    # by default, jupyter port is '8888'
    ssh -L <localport>:localhost:<jupyter-port> user@al-dev
    
The provided ``Tutorial Service`` notebook provides a very basic of how to register and run a service 
from within a jupyter notebook
